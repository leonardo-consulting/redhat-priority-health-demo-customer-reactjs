// import React from 'react';
// import Container from '@material-ui/core/Container';
// import Typography from '@material-ui/core/Typography';
// import Box from '@material-ui/core/Box';
// import Link from '@material-ui/core/Link';
// import ProTip from './ProTip';
// import './style.css';

// function Copyright() {
//   return (
//     <h1> test </h1>
//   );
// }

// const style = {
//   paddingLeft: "220px",
// };

// export default function App() {
//   return (
//     <div className="header-basic">
//       <img src={require('./PHlogo.jpg')} alt="logo" height="34" width="169" ></img>
//     </div>
//   );
// }



import React from 'react';
import { fade, makeStyles } from '@material-ui/core/styles';
import InputBase from '@material-ui/core/InputBase';

import SearchIcon from '@material-ui/icons/Search';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Container from 'react-bootstrap/Container';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.css';

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    color: "#BDBDBD"
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex',
    },
  },

}));



export default function PrimarySearchAppBar() {
  const [anchorEl, setAnchorEl] = React.useState(null);

const handleClick = event => {
  setAnchorEl(event.currentTarget);
};

const handleClose = () => {
  setAnchorEl(null);
};

  const classes = useStyles();

  return (
    <div className={classes.grow}>
<div className="header-basic">
  <Container>
    <Row>
        <Col>
        <img src={require('./PHlogo.jpg')} alt="logo" height="34" width="169"></img>
        </Col>
        <Col xs={7}>
        <Grid container alignItems="center" className={classes.root}>

        <InputBase
              placeholder="Search…"
              classes={{
                root: classes.inputRoot,
                input: classes.inputInput,
              }}
              inputProps={{ 'aria-label': 'search' }}
              />
<Button>
<SearchIcon style={{fill: "BDBDBD"}}/>
</Button>

              <div style={{paddingLeft:"10px"}} />
<Divider orientation="vertical" flexItem />


<div style={{paddingLeft:"35px"}} />
                <MailIcon style={{fill: "66B057"}} />

                <div style={{paddingLeft:"29px"}} />
                <NotificationsIcon style={{fill: "66B057"}} />
                <div style={{paddingLeft:"29px"}} />
                <div>
      <Button aria-controls="simple-menu" aria-haspopup="true" onClick={handleClick}>
      <span style={{color:"#026642"}} >Robb Harvey  <ArrowDropDownIcon /></span>
      </Button>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={handleClose}>Profile</MenuItem>
        <MenuItem onClick={handleClose}>My account</MenuItem>
        <MenuItem onClick={handleClose}>Logout</MenuItem>
      </Menu>
    </div>

              
        </Grid>
              </Col>
              </Row>
              </Container>
</div>
    </div>
  );
}