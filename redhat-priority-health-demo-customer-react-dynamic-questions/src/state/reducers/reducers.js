import { updateObject } from './../../shared/utility';
import * as actionTypes from '../actions/actions';

 const initialState = {
    notificationList: [],
    transactionHistory: [],
    transactionDisputes: [],
    transactionDisputeReasonQuestions: [],
    transactionDisputeReasonAnswers: "",
    creditCardQAs: {},
    additionalQuestions: [],
    additionalQuestionAnswers: [],
    extraInfos: null,
    submitDisputes: null,
    loading: false,
    error: false,
};

const notificationLists = (state, action) => {
    return updateObject(state, {notificationList: action.payload});
};
const transactionHistories = (state, action) => {
    return updateObject(state, {transactionHistory: action.payload});
};
const transactionDisputes = (state, action) => {
    return updateObject(state, {transactionDisputes: action.payload});
};
const transactionDisputeReasonQuestions = (state, action) => {
    return updateObject(state, {transactionDisputeReasonQuestions: action.payload});
};
const transactionDisputeReasonAnswers = (state, action) => {
    return updateObject(state, {transactionDisputeReasonAnswers: action.payload});
};
const creditCardQAs = (state, action) => {
    return updateObject(state, {creditCardQAs: action.payload});
};
const additionalQuestions = (state, action) => {
    return updateObject(state, {additionalQuestions: action.payload});
};
const additionalQuestionAnswers = (state, action) => {
    return updateObject(state, {additionalQuestionAnswers: action.payload});
};
const extraInfos = (state, action) => {
    return updateObject(state, {extraInfos: action.payload});
};
const submitDisputes = (state, action) => {
    return updateObject(state, {submitDisputes: action.payload});
};
const restCallFail = (state, action) => {
    return updateObject(state, { loading: false, error: true });
};

const restCallStart = (state, action) => {
    return updateObject(state, { loading: true, error: false });
};

const restCallSuccess = (state, action) => {
    return updateObject(state, { loading: false, error: false });
};
const transactionDisputeReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.GET_NOTIFICATION_HISTORY: return notificationLists(state, action);
        case actionTypes.GET_TRANSACTION_HISTORY: return transactionHistories(state, action);
        case actionTypes.ADD_DISPUTE: return transactionDisputes(state, action);
        case actionTypes.GET_DISPUTE_REASON_QUESTIONS: return transactionDisputeReasonQuestions(state, action);
        case actionTypes.SET_DISPUTE_REASON_ANSWER: return transactionDisputeReasonAnswers(state, action);
        case actionTypes.ADD_CREDIT_CARD_QUESTION_ANSWER: return creditCardQAs(state, action);
        case actionTypes.GET_ADDITIONAL_QUESTIONS: return additionalQuestions(state, action);
        case actionTypes.SET_ADDITIONAL_QUESTIONS_ANSWER: return additionalQuestionAnswers(state, action);
        case actionTypes.SET_EXTRA_INFO: return extraInfos(state, action);
        case actionTypes.SUBMIT_DISPUTE: return submitDisputes(state, action);
        case actionTypes.REST_CALL_START: return restCallStart(state, action);
        case actionTypes.REST_CALL_FAIL: return restCallFail(state, action);
        case actionTypes.REST_CALL_SUCCESS: return restCallSuccess(state, action);
        default: return state;
    }
};

export default transactionDisputeReducer;
