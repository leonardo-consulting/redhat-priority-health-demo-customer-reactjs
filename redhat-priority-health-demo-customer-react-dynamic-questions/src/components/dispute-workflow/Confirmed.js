/* eslint no-unused-vars: 0, eqeqeq:0 */
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';


class Confirmed extends Component {
    handleSubmit = () => {
        // e.preventDefault();
        this.props.history.push('/');
    }
    render() {
        return (
            <div>
                <br />
                <div className="row">
                    <div className="col-lg-12">
                        <h3> Confirmed</h3>
                    </div>
                    <br />
                </div>
                <div className="row">
                    <div className="col-lg-12">
                        <label>Your Dispute Request ID is: <b>{this.props.submitDisputesResult}</b></label>
                        <p>Thanks you for your submission. Your dispute request has been successfully logged. You can review your request at any time in your notification list in the menu bar. </p>
                        <p>You should recieve an email shortly indicating next steps.</p>
                        <p>If you want, you can use the ID Number to check or ask more information about the process of your request.</p>
                        <p>Regards,</p>
                    </div>
                    <br />
                </div>
                <div className="row">
                    <Link
                        className="btn btn-primary DecisionQuestionApp__custom-buttons pull-right stepZillRightabt"
                        to="/">Home
             </Link>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        submitDisputesResult: state.transactionDisputeReducer.submitDisputes
    }
}
export default connect(mapStateToProps)(Confirmed);
