import React, { Component } from 'react';
import { connect } from 'react-redux';

import { setExtraInfo } from '../../state/actions/actions';


class ExtraInfo extends Component {

    handleUserInput = (e) => {
        const value = e.target.value;
        this.props.setExtraInfo(value);
    }
    render() {
        return (
            <div>
                <br />
                <div className="row">
                    <div className="col-lg-8">
                        <label className="DecisionQuestionApp__step">
                            <b>3. Is there anything else you&aposd like to tell us about this dispute?</b>
                        </label>
                        <div className="DecisionQuestionApp__spacer">
                            <textarea className="form-control" rows="3" name="usertext" placeholder="Type here..." value={this.props.extraInfos} onChange={(e) => this.handleUserInput(e)} ></textarea>
                        </div>
                    </div>
                    <div className="col-lg-2">
                    </div>
                    <br />
                </div>
                <div className="row">
                    <button name="prev" type="button" className="btn btn-sm pull-right stepZillLeftabt" onClick={() => this.props.jumpToStep(1)} ><i className="fa fa-chevron-left"></i></button>
                    {this.props.extraInfos == "" ?
                        <button name="next" disabled type="button" className="btn btn-primary  pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(5)} >Next</button>
                        :
                        <button name="next" type="button" className="btn btn-primary  pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(5)} >Next</button>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        extraInfos: state.transactionDisputeReducer.extraInfos,
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setExtraInfo: (payload) => dispatch(setExtraInfo(payload)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(ExtraInfo);
