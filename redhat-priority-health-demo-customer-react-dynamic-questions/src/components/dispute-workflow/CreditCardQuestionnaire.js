import React, { Component } from 'react';
import { connect } from 'react-redux';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { updateObject } from './../../shared/utility';

import { addCreditCardQuestionAnswer } from '../../state/actions/actions';


class CreditCardQuestionnaire extends Component {

    handleUserAnswer = (name, value) => {

        const currentState = this.props.creditCardQAs;
        let newState;
        let date;

        switch (name) {
            case "missingDate":
                date = value;
                newState = updateObject(currentState, { missingDate: date, confirmed: true });
                this.props.addCreditCardQuestionAnswer(newState);
                break;
            case "lastUsedDate":
                date = value;
                newState = updateObject(currentState, { lastUsedDate: date, confirmed: true });
                this.props.addCreditCardQuestionAnswer(newState);
                break;

            default:
                newState = updateObject(currentState, { [name]: value, confirmed: true });
                this.props.addCreditCardQuestionAnswer(newState);
                break;
        }

    }

    render() {
        const currentValues = this.props.creditCardQAs;

        const yesBt = currentValues.creditCardwithCustomer === "Yes" ?
            <input name="yes" type="button" className="btn btn-sm  btn-primary DecisionQuestionApp__custom-buttons neibourb active" onClick={(name, value) => this.handleUserAnswer("creditCardwithCustomer", "Yes")} value="Yes" /> :
            <input name="yes" type="button" className="btn btn-sm btn-default DecisionQuestionApp__custom-buttons neibourb" onClick={(name, value) => this.handleUserAnswer("creditCardwithCustomer", "Yes")} value="Yes" />;

        const noBt = currentValues.creditCardwithCustomer === "No" ?
            <input name="no" type="button" className="btn btn-primary DecisionQuestionApp__custom-buttons btn-sm neibourbt active" onClick={(name, value) => this.handleUserAnswer("creditCardwithCustomer", "No")} value="No" /> :
            <input name="no" type="button" className="btn btn-sm DecisionQuestionApp__custom-buttons btn-default neibourbt" onClick={(name, value) => this.handleUserAnswer("creditCardwithCustomer", "No")} value="No" />;

        let showOrNot = null;
        if (currentValues.creditCardwithCustomer === "No") {
            showOrNot =
                <div>
                    <div className="row">
                        <div className="form-inline col-lg-12">
                            <div className="form-group">
                                <label className="DecisionQuestionApp__step">When was your Credit Card last in your posession?</label>
                            </div>
                        </div>
                    </div>

                    <div className="row">
                        <div className="col-lg-5">
                            <p className="DecisionQuestionApp__spacer DecisionQuestionApp__quest-p">What date did you realize the card(s) were missing?</p>
                        </div>
                        <div className="col-lg-5">
                            <DatePicker
                                name="missingDate"
                                selected={currentValues.missingDate}
                                onChange={(date) => this.handleUserAnswer('missingDate', date)}
                                disabledKeyboardNavigation
                                placeholderText="Click to select a date"

                            />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-5">
                            <p className="DecisionQuestionApp__spacer DecisionQuestionApp__quest-p">What date did you last use the card?</p>
                        </div>
                        <div className="col-lg-5">
                            <DatePicker
                                name="lastUsedDate"
                                selected={currentValues.lastUsedDate}
                                onChange={(date) => this.handleUserAnswer('lastUsedDate', date)}
                                disabledKeyboardNavigation
                                placeholderText="Click to select a date"
                            />
                        </div>
                    </div>
                </div>;
        }

        return (
            <div>
                <div>
                    <div>
                        <br />
                        <div className="row">
                            <div className="col-lg-12">
                                <label className="DecisionQuestionApp__step">2. Has your credit card been with you the entire time?</label>
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-inline col-lg-12 DecisionQuestionApp__spacer">
                                <div className="form-group">
                                    {yesBt}
                                </div>
                                <div className="form-group">
                                    {noBt}
                                </div>
                            </div>
                        </div>
                        <br />
                        {showOrNot}
                    </div>
                    <br />
                </div>
                <div className="row">
                    <button name="prev" type="button" className="btn btn-primary  pull-left stepZillLeftabt" onClick={() => this.props.jumpToStep(1)} ><i className="fa fa-chevron-left"></i></button>
                    {
                        currentValues.confirmed == true ?
                            <button name="next" type="button" className="btn btn-primary  pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(3)} >Next</button>
                            :
                            <button name="next" type="button" disabled className="btn btn-primary  pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(3)} >Next</button>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        creditCardQAs: state.transactionDisputeReducer.creditCardQAs
    }
}
const mapDispatchToProps = dispatch => {
    return {
        addCreditCardQuestionAnswer: (payload) => dispatch(addCreditCardQuestionAnswer(payload)),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(CreditCardQuestionnaire);
