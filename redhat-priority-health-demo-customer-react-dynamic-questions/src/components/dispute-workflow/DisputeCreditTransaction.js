import React, { Component } from 'react';
import { connect } from 'react-redux';
import currencyFormatter from 'currency-formatter';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';


class DisputeCreditTransaction extends Component {

    state = {
        selectedAnswer: "Select an answer ...",
    }

    confirmed = () => {
        var jsonObj = this.state;
        jsonObj.confirmCheck = true;
        this.setState({
            jsonObj
        });
    }

    
    // handleChange(event) {
    //     this.setState({
    //         selectedAnswer: event.target.value
    //     })
    // }

    handleChange = (event) => {
        console.log(this.state.selectedAnswer);
        this.setState({
            selectedAnswer: event.target.value
        })
      }

    render() {

        return (
            <div>
                <div className="row" >
                    <br />
                    <div className="col-sm-12">
                        <p className="DecisionQuestionApp__question">Please confirm the following transactions in question</p>
                        <label className="DecisionQuestionApp__step"><b>Hello, my name is Robb. Thank you for calling Priority Health. What do I have the pleasure of helping you with today?</b></label>
                    




            <br />
            <br />

        <NativeSelect
          value={this.state.selectedAnswer}
          onChange={this.handleChange}
          style={{width:"98%"}}
        >
          <option value={""} selected disabled>Select an answer</option>
          <option value={"File a claim"}>File a claim</option>
          <option value={"Get a replacement insurance card"}>Get a replacement insurance card</option>
          <option value={"Change my physisican"}>Change my physisican</option>
          <option value={"Change my address"}>Change my address</option>
          <option value={"Other"}>Other</option>
        </NativeSelect>



            <br />
            <br />
            <br />



                    </div>
                </div>
                <div className="row">
                    <div className="DecisionQuestionApp__spacer">
                        <button name="confirm" type="button" className="btn DecisionQuestionApp__custom-buttons btn-primary" onClick={() => this.confirmed()} >Yes - This is correct</button>
                        <button name="cancel" type="button" className="btn DecisionQuestionApp__custom-buttons btn-default " onClick={() => this.props.history.push('/')} >No - Cancel</button>
                    </div>
                </div>
                <div className="row">
                    {
                        this.state.confirmCheck === true ?
                            <button name="next" type="button" className="btn btn-primary pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(1)} >
                                Next</button>
                            :
                            <button name="next" disabled type="button" className="btn btn-default pull-right stepZillRightabt" onClick={() => this.props.jumpToStep(1)} >
                                Next</button>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        transactionDisputes: state.transactionDisputeReducer.transactionDisputes
    }
}


export default connect(mapStateToProps)(DisputeCreditTransaction);
